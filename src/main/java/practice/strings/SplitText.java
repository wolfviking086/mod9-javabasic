package practice.strings;

import java.util.regex.Pattern;

public class SplitText {
    private static Pattern pattern = Pattern.compile("[\\s\\-\\;\\.\\, 0-9]+");
    
    public static String splitTextIntoWords(String text){
        String[] words = pattern.split(text);
        String joinedString = String.join("\n", words);
        return joinedString;
    }
}

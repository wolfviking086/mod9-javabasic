package practice.regex;


import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FullNameFormatterRegex {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            Pattern pattern = Pattern.compile("\\d");
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                System.out.println("Введенная строка не является ФИО");
                break;
            }
            int count = 0;
            if (input.length() != 0) {
                count++;
                for (int i = 0; i < input.length(); i++) {
                    if (input.charAt(i) == ' ') {
                        count++;
                    }
                }
            }
            if (count != 3) {
                System.out.println("Введенная строка не является ФИО");
                break;
            }
            
            String name = input;
            System.out.println(
                    "Фамилия: " + name.substring(0, name.indexOf(' ')) + System.lineSeparator() +
                            "Имя:" + name.substring(name.indexOf(' '), name.lastIndexOf(' ')) + System.lineSeparator() +
                            "Отчество:" + name.substring(name.lastIndexOf(' ')));
            break;


        }
    }
}

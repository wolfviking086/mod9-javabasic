package practice.regex;

import java.util.Scanner;

public class PhoneCleanerRegex {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        input = input.replaceAll("[^0-9]", "");


        while (true) {
            if (input.length() < 10 || input.length() > 14) {
                System.out.println("Неверный формат номера");
                break;
            } else if (input.startsWith("8") || input.startsWith("7")) {
                input = input.substring(1);
                input = 7 + input;
                System.out.println(input);
                break;
            } else if (input.length() == 10) {
                input = input.substring(0);
                input = 7 + input;
                System.out.println(input);
                break;
            } else {
                System.out.println("Неверный формат номера");
                break;
            }


        }
    }
}
